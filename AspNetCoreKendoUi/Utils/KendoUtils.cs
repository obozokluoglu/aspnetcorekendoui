﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using System;
using System.Linq;

namespace AspNetCoreKendoUi.Utils
{
    public static class KendoUtils
    {
        public static string GetJsonDataFromQueryString(this HttpContext httpContext)
        {
            var rawQueryString = httpContext.Request.QueryString.ToString();
            var rawQueryStringKeyValue = QueryHelpers.ParseQuery(rawQueryString).FirstOrDefault();
            var dataString = Uri.UnescapeDataString(rawQueryStringKeyValue.Key);
            return dataString;
        }
    }
}
